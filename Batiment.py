#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import fileinput
import Salle  

class Batiment: # <=> GestionSalles
    """Classe définissant un batiment caractérisé par :
    - une adresse"""


    typesSalleArray = {1:'cuisine', 2:'vaisselle uniquement', 3:'video', 4:'tableau', 5:'amphitheatre'}

    def __init__(self, num, nom, adr_num, adr_rue, adr_cp, adr_ville):
        self.id=int(num) #int
        self.nom = nom #string
        self.adr_num = int(adr_num) #int
        self.adr_rue= adr_rue #string
        self.adr_cp= int(adr_cp) #int
        self.adr_ville= adr_ville #string
        self.adresse= str(adr_num)+" "+str(adr_rue)+" "+str(adr_cp)+" "+str(adr_ville) #string
        self.sallesArray = [] #array

    #Fonction d'ajout de Salle avec vérif
    #return un entier correspondant à une erreur
    #param salle_num     : int : numéro de la salle
    #param salle_etage   : int : étage auquel se trouve la salle
    #param self.id       : int : batiment dans lequel se trouve la salle
    #param salle_type    : string: type de la salle
    #param superficie    : int : superficie de la salle (m²)
    #param materiel      : array : liste du materiel présent dans la salle (None si aucun)
    #param cuisine       : boolean : indique si la salle est équipée d'une cuisine
    def addSalle(self,salle_etage, salle_num, salle_type, superficie, materiel, cuisine):
        idSalleArray = self.getAllIdSalles()

        code_retour = 0 # Test OK   

        #L'identifiant de la salle doit être unique
        id_salle=str(salle_etage)+"-"+str(salle_num)+"-"+str(self.id)
        if (str(id_salle) in idSalleArray):
            code_retour = 1 #Identifiant déjà existant
            print "\nCréation impossible, une salle existe déjà à cet emplacement"

        #Le type de salle choisit doit exister
        if (int(salle_type)<1 or int(salle_type)>len(Batiment.typesSalleArray)):
            code_retour = 2 #Erreur Type Salle
            print "\nCréation impossible, veuillez saisir un type correct"
        else :
            salle_type=Batiment.typesSalleArray.get(int(salle_type))

        #On n'ajoute la salle que si aucune erreur n'a été détectée
        if (code_retour == 0) :
            self.addSalleBat(Salle.Salle(salle_etage,salle_num,self.id,salle_type,superficie,materiel,cuisine))
            print "\nSalle ", id_salle, " creee avec succes"

        return code_retour     

    #   ajouter une salle à un batiment
    def addSalleBat(self,salle):
        self.sallesArray.append(salle)
     
    def getAdresse(self):
        print "Adresse : " + str(self.adresse)

    def getId(self):
        return int(self.id)

    def getAllSalles(self):
        salleArray = []
        for salle in self.sallesArray:
            salleArray.append(salle)
        return salleArray

    def getAllIdSalles(self):
        salleIdArray = []
        for salle in self.sallesArray:
            salle_id = str(salle.etage)+"-"+str(salle.num)+"-"+str(self.id)
            salleIdArray.append(salle_id)
        return salleIdArray

    def getSalle(self, idSalle):
        splitIdSalle = idSalle.split("-")
        idSalleEtage = int(splitIdSalle[0])
        idSalleNum = int(splitIdSalle[1])
        idSalleBat = int(splitIdSalle[2])
        for salle in self.sallesArray:
            if (salle.etage == idSalleEtage and salle.num == idSalleNum and salle.bat == idSalleBat):
                return salle

    def setNom(self, nom):
        self.nom=nom

    def setAdrNum(self, adr_num):
        self.adr_num=adr_num

    def setAdrRue(self, adr_rue):
        self.adr_rue=adr_rue

    def setAdrCp(self,adr_cp):
        self.adr_cp=adr_cp

    def setAdrVille(self, adr_ville):
        self.adr_ville=adr_ville

    def setAdresse(self,adr_num, adr_rue, adr_cp, adr_ville):
        self.adresse= str(adr_num)+" "+str(adr_rue)+" "+str(adr_cp)+" "+str(adr_ville) #string

    def __str__(self):
        listeSalleStr = ""
        for salle in self.sallesArray :
            listeSalleStr = listeSalleStr+("   --> Salle "+str(salle.etage)+"-"+str(salle.num)+"-"+str(salle.bat)+" ("+str(salle.type)+") : "+str(salle.superficie)+"m² avec comme materiel : "+str(salle.materielArray)+"\n")
        return str("Le batiment "+str(self.id)+" : "+self.nom+" ("+self.adresse+") contient "+str(len(self.sallesArray))+" salle(s) : \n"+listeSalleStr)
    


