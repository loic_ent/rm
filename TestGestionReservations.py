#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import unittest

import GestionGenerale

class TestGestionReservations(unittest.TestCase): #Attention, les fonctions de test s'exécutent dans l'ordre alphabétique

	gg=GestionGenerale.GestionGenerale()

#Test de la fonction d'ajout de réservation :

	def test1_addReservation(self):
		#Ajouts de Demandeurs
		res = TestGestionReservations.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 1, 2) # id généré : 1
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 1, 2) # id généré : 2
		self.assertEqual(res,0)

		#Ajouts de Batiments
		res = TestGestionReservations.gg.getGestionBatiments().addBatiment(1,"Maison des projets", "39","rue des maraichers",44000, "Nantes")
		self.assertEqual(res,0)            
		res = TestGestionReservations.gg.getGestionBatiments().addBatiment(2,'Anatole France', '20', 'rue des maraichers', 44800 , 'Saint-Herblain')
		self.assertEqual(res,0)

		#Ajouts de Salles
		res = TestGestionReservations.gg.getGestionBatiments().addSalle(1, 1, 1, 2, 30, ["video", "pieuvre"], 0) #id : '1-1-1'
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.getGestionBatiments().addSalle(1, 1, 2, 3, 20, ["video"], 0) #id : '1-1-2'
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.getGestionBatiments().addSalle(1, 2, 1, 1, 35, ["video"], 1) #id : '1-2-1'
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.getGestionBatiments().addSalle(1, 3, 1, 5, 20, [], 0) #id : '1-3-1'
		self.assertEqual(res,0)


		#Ajout OK
		res = TestGestionReservations.gg.addReservation(1, '1-1-1', 20150305, 'soirée')
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.addReservation(1, '1-1-1', 20150305, 'demi-journée')
		self.assertEqual(res,0)
		res = TestGestionReservations.gg.addReservation(1, '1-1-2', 20150305, 'demi-journée')
		self.assertEqual(res,0)

		#Salle déjà reservée
		res = TestGestionReservations.gg.addReservation(1, '1-1-1', 20150305, 'soirée')
		self.assertEqual(res,1)

		#Id demandeur Inexistant
		res = TestGestionReservations.gg.addReservation(4, '1-1-1', 20150305, 'soirée')
		self.assertEqual(res,2)

		#Id salle inexistant
		res = TestGestionReservations.gg.addReservation(1, '4-5-6', 20150305, 'soirée')
		self.assertEqual(res,3)

	def test2_affichage(self):
		print ('\n=========================================================================')
		TestGestionReservations.gg.getGestionReservations().printAllReservations()
		print ('=========================================================================\n')


#Test de la fonction d'annulation de réservation :
	def test3_deleteReservation(self):
		#OK
		res = TestGestionReservations.gg.getGestionReservations().deleteReservation('1-1-1', 20150305, 'soirée')
		self.assertEqual(res,0)

		#Resa inexistante
		res = TestGestionReservations.gg.getGestionReservations().deleteReservation('1-1-5', 20150305, 'soirée')
		self.assertEqual(res,1)

	def test9_affichage(self):
		print ('\n=========================================================================')
		TestGestionReservations.gg.getGestionReservations().printAllReservations()
		print ('=========================================================================\n')

if __name__ == '__main__':
    unittest.main()
