# -*- coding: utf-8 -*-

import Materiel

class Salle:
    """Classe définissant une salle caractérisée par :
    - une adresse
    - un nombre de salle"""


    def __init__(self, salle_etage, salle_num, salle_bat, salle_type, superficie, materiel, cuisine):
        #self.id = salle_etage)+"-"+str(salle_num)+"-"+str(salle_bat)
        self.etage = int(salle_etage) # int
        self.num= int(salle_num) # int
        self.bat = int(salle_bat) # int
        self.type = salle_type #string
        self.superficie = int(superficie)#int
        self.materielArray=[]
        for m in materiel:
            self.addMateriel(m)
        self.cuisine = cuisine #bool

    def addMateriel(self, mat):
        self.materielArray.append(mat)

    def removeMateriel(self, mat):
        self.materielArray.remove(mat)
    
    def getMateriel(self):
        return self.materielArray

    def __str__(self):
        return str("Salle "+str(self.etage)+"-"+str(self.num)+"-"+str(self.bat)+" ("+str(self.type)+") : "+str(self.superficie)+"m²")

        # def isLibre(self, dateDebut, dateFin):

        #Fonction : ajout matériel fixe
