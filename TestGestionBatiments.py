#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import unittest


import GestionGenerale
import Batiment

class TestGestionBatiments(unittest.TestCase): #Attention, les fonctions de test s'exécutent dans l'ordre alphabétique

      gg=GestionGenerale.GestionGenerale()

#Test de la fonction d'ajout de batiment :

      def test1_addBatiment(self):
            #Ajouts OK
            res = TestGestionBatiments.gg.getGestionBatiments().addBatiment(1,"Maison des projets", "39","rue des maraichers",44000, "Nantes")
            self.assertEqual(res,0)            
            res = TestGestionBatiments.gg.getGestionBatiments().addBatiment(2,'Anatole France', '20', 'rue des maraichers', 44800 , 'Saint-Herblain')
            self.assertEqual(res,0)

            #Ajout impossible : Id déjà existant
            res = TestGestionBatiments.gg.getGestionBatiments().addBatiment(1,"Batiment 1 ", "12","rue des marrons",44000, "Nantes")
            self.assertEqual(res,1)


      def test1_updateBatiment(self):
            #Ajouts OK
            res = TestGestionBatiments.gg.getGestionBatiments().updateBatiment(1,"Maison des projets 'mise a jour'", "42","rue des maraichers mise a jour",4444, "Naoned")
            self.assertEqual(res,0)
            #Ajouts pas OK
            res = TestGestionBatiments.gg.getGestionBatiments().updateBatiment(4,"Maison des projets 'mise a jour'", "42","rue des maraichers mise a jour",4444, "Naoned")
            self.assertEqual(res,1)
          
          

#Test de la fonction d'ajout de salle

      def test2_addSalle(self):
            #Ajouts OK
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 1, 1, 2, 30, ["retro-projecteur"], 0)
            self.assertEqual(res,0)
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 1, 2, 3, 20, [], 0) #meme etage; meme num; mais bat différent
            self.assertEqual(res,0)

            #Id déjà pris
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 1, 2, 2, 30, [], 1) #meme etage; meme num; mais bat différent
            self.assertEqual(res,1)

            #Type de salle
                  #incorrects
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 2, 1, 0, 30, [], 0) # 4ème param = 0
            self.assertEqual(res,2)
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 3, 1, 6, 30, [], 1) # 4ème param = 6
            self.assertEqual(res,2)
                  #limite
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 2, 2, 1, 35, [2], 1) # 4ème param = 1
            self.assertEqual(res,0)
            res = TestGestionBatiments.gg.getGestionBatiments().addSalle(1, 3, 1, 5, 20, [], 0) # 4ème param = 5
            self.assertEqual(res,0)

# test de la fonction d'ajout de materiel à une salle
      def test3_addMaterielToSalle(self):
            res = TestGestionBatiments.gg.getGestionBatiments().addMaterielToSalle(1,1,1, "Xbox")
            self.assertEqual(res,0)
            res = TestGestionBatiments.gg.getGestionBatiments().addMaterielToSalle(13,4,4, "Xbox")
            self.assertEqual(res,1)

# test de la fonction de listage du materiel d'une salle
      def test4_listMaterielOfSalle(self):
            res = TestGestionBatiments.gg.getGestionBatiments().listMaterielOfSalle(1,1,1)
            self.assertEqual(res,0)

            res = TestGestionBatiments.gg.getGestionBatiments().listMaterielOfSalle(34,1,1)
            self.assertEqual(res,1)

# test de la fonction d'ajout de materiel à une salle
      def test5_removeMaterielFromSalle(self):
            res = TestGestionBatiments.gg.getGestionBatiments().removeMaterielFromSalle(1,1,1,"Xbox")
            self.assertEqual(res,0)
            TestGestionBatiments.gg.getGestionBatiments().listMaterielOfSalle(1,1,1)

#Test de la fonction de recherche d'un batiment :

      def test5_rechercherBatiment(self):
            #Batiment introuvable
            res = TestGestionBatiments.gg.getGestionBatiments().rechercherBatiment(4)
            self.assertEqual(res,1)

            #recherche OK
            res = TestGestionBatiments.gg.getGestionBatiments().rechercherBatiment(1)
            self.assertEqual(res,0)

#Test de la fonction de recherche d'une salle :

      def test6_rechercherSalle(self):
            #Salle introuvable
            res = TestGestionBatiments.gg.getGestionBatiments().rechercherSalle('3-8-9')
            self.assertEqual(res,1)

            #recherche OK
            res = TestGestionBatiments.gg.getGestionBatiments().rechercherSalle('1-1-1')
            self.assertEqual(res,0)


      def test7_affichage(self):
            print ('\n=========================================================================')
            TestGestionBatiments.gg.getGestionBatiments().printAllBatiments()
            print ('=========================================================================\n')

#Test de la fonction de suppression d'un batiment :

      def test8_deleteBatiment(self):
            #OK
            res = TestGestionBatiments.gg.getGestionBatiments().deleteBatiment(1)
            self.assertEqual(res,0)

            #Batiment inexistant
            res = TestGestionBatiments.gg.getGestionBatiments().deleteBatiment(8)
            self.assertEqual(res,1)

#Test de la fonction de suppression d'une salle :

      def test9_deleteSalle(self):
            #OK
            res = TestGestionBatiments.gg.getGestionBatiments().deleteSalle('1-1-2')
            self.assertEqual(res,0)

            #Salle inexistante
            res = TestGestionBatiments.gg.getGestionBatiments().deleteSalle('5-5-5')
            self.assertEqual(res,1)

      def test10_affichage(self):
            print ('\n=========================================================================')
            TestGestionBatiments.gg.getGestionBatiments().printAllBatiments()
            print ('=========================================================================\n')




if __name__ == '__main__':
    unittest.main()
