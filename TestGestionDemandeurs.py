#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import unittest

import GestionGenerale

class TestGestionDemandeurs(unittest.TestCase): #Attention, les fonctions de test s'exécutent dans l'ordre alphabétique

    gg=GestionGenerale.GestionGenerale()

#Test de la fonction d'ajout de demandeur :

    def test1_addDemandeur(self):
        #Ajouts OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 1, 2)
        self.assertEqual(res,0)

        #Titre du demandeur
        	#incorrect
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 0, 2) #3ème param = 0
        self.assertEqual(res,1)
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 4, 2) #3ème param = 4
        self.assertEqual(res,1)
        	#limite
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Loic", "Entressangle", 1, 2) #3ème param = 1
        self.assertEqual(res,0)
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 3, 2) #3ème param = 3
        self.assertEqual(res,0)

        #Origine du demandeur
        	#incorrect
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 1, 0) #4ème param = 0
        self.assertEqual(res,2)
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 2, 3) #4ème param = 3
        self.assertEqual(res,2)
        	#limite
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Gerson", "Sunye", 3, 1) #4ème param = 1
        self.assertEqual(res,0)
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().addDemandeur("Marion", "Guillet", 1, 2) #4ème param = 2
        self.assertEqual(res,0)

    def test2_affichage(self):
        print ('\n=========================================================================')
        TestGestionDemandeurs.gg.getGestionDemandeurs().printAllDemandeurs()
        print ('=========================================================================\n')

#Test de la fonction d'annulation de réservation :
    def test3_deleteDemandeur(self):
        #OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().deleteDemandeur(1)
        self.assertEqual(res,0)

        #Resa inexistante
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().deleteDemandeur(10)
        self.assertEqual(res,1)

    def test4_updateDemandeur(self):
        #Test de la fonction setNomDemandeur
            #OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setNomDemandeur(3, 'Marquer')
        self.assertEqual(res,0)
            #idDemandeur inexistant
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setNomDemandeur(8, 'Jacques')
        self.assertEqual(res,1)

        #Test de la fonction setPrenomDemandeur
            #OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setPrenomDemandeur(3, 'Adeline')
        self.assertEqual(res,0)
            #idDemandeur inexistant
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setPrenomDemandeur(8, 'Jacques')
        self.assertEqual(res,1)

        #Test de la fonction setTitreDemandeur
            #OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setTitreDemandeur(2,2)
        self.assertEqual(res,0)
            #Titre KO
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setTitreDemandeur(4,8)
        self.assertEqual(res,2)
            #idDemandeur inexistant
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setTitreDemandeur(8,1)
        self.assertEqual(res,1)

        #Test de la fonction setOrigineDemandeur
            #OK
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setOrigineDemandeur(2,1)
        self.assertEqual(res,0)
            #Origine KO
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setOrigineDemandeur(3,9)
        self.assertEqual(res,2)
            #idDemandeur inexistant
        res = TestGestionDemandeurs.gg.getGestionDemandeurs().setOrigineDemandeur(8,1)
        self.assertEqual(res,1)


    def test5_affichage(self):
        print ('\n=========================================================================')
        TestGestionDemandeurs.gg.getGestionDemandeurs().printAllDemandeurs()
        print ('=========================================================================\n')

if __name__ == '__main__':
    unittest.main()