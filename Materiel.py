#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

class Materiel:
    """Classe définissant un materiel caractérisé par :
    - un identifiant"""

    i = int(1) # incrément pour les identifiants

    def __init__(self,type):
        self.id = Materiel.i
        self.type = type
        Materiel.i+=1

        def getId(self):
            print str(self.id)