#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import datetime

class Reservation:
    """Classe définissant une reservation caractérisé par :
    - un identifiant de salle
    - une date de début / date de fin
    - un montant """

    #param id          : string : concaténation de l'id de la salle, de la date et de la durée
    #param idDemandeur : string: id de la salle à reserver
    #param idSalle     : string: id de la salle à reserver
    #param date        : string : type 20150315 (pour 15 mars 2015)
    #param duree       : string : 'demie-journée' ou 'soirée'
    #param montant     : int : attribut dérivé
    def __init__(self, idDemandeur, idSalle, date, duree):
        self.id = str(idSalle)+str(date)+str(duree)
        self.idSalle=idSalle #string
        self.date=date #string
        self.duree=duree #string
        self.montant = 0 #int


    #Calcul du montant de la réservation
    # à appeler après chaque création de réservation
    def calculMontant(self, salle, duree):
        # 1 materiel = 5€
        # 1 m² = 3€
        # 1 cuisine = 30€
        # demi-journée = 40€, soirée = 60€

        # type de salle : prixType
        nbMateriel = 0
        if (len(salle.materielArray) > 0) :
            for mat in salle.materielArray :
                nbMateriel+=1

        superficie = int(salle.superficie)
        cuisine = int(salle.cuisine)
        typeS = salle.type

        if (typeS == 'cuisine') :
            prixType = 20
        elif (typeS == 'vaisselle uniquement') :
            prixType = 15
        elif (typeS == 'video') :
            prixType = 60
        elif (typeS == 'tableau') :
            prixType = 60
        elif (typeS == 'amphitheatre') :
            prixType = 150
        else :
            prixType = 0

        if (duree == 'demi-journée'):
            prixDuree = 40
        elif (duree == 'soirée') :
            prixDuree = 60

        #Montant final
        montant = nbMateriel*5 + superficie*3 + cuisine*30 + prixDuree + prixType

        self.montant = montant

    def getId(self):
        return self.id

    def __str__(self):
        dateResa = datetime.datetime.strptime(self.date, '%Y%m%d')
    	return str("Salle num "+str(self.idSalle)+" -> reservee le  "+dateResa.strftime('%d %b %Y')+" pour la  "+str(self.duree+" ==> Montant : "+str(self.montant)+"€")+"\n")