#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import Reservation

import datetime

class GestionReservations:
    """Classe définissant un planning caractérisé par :
    - une liste de réservation """

    def __init__(self):
        self.resaArray = []

    #Vérifie que les paramètres saisis sont corrects, calcul le montant et
    #crée la réservation

    ##TO DO 
    ##Faire l'ajout par ordre chronologique au planning : Non : juste l'affichage
    def addReservation(self, idDemandeur, idSalle, date, duree, idSalleArray, idDemandeurArray, existSalle, existDemandeur, salle) :
        code_retour = 0
        #On vérifie que la réservation concerne une salle et un demandeur existants
        if (duree != 'soirée' and duree != 'demi-journée'):
            code_retour = 4 #Duree invalide
            print "\nLa duree saisie ne peut-être que 'demi-journée' ou 'soirée'"
        else : 
            if (existSalle) : 
                if (existDemandeur) :
                    splitIdSalle = idSalle.split("-")
                    idSalleEtage = splitIdSalle[0]
                    idSalleNum = splitIdSalle[1]
                    idSalleBat = splitIdSalle[2]

                    idResa = str(idSalle)+str(date)+str(duree)

                    #Verification de la disponibilité de la Salle
                    if (self.existReservation(idResa)) : 
                        code_retour = 1 #Salle déjà reservée
                        print "\nRéservation impossible, salle déjà reservée à date et pour la durée souhaitée"

                    else :
                        #Création de la réservation
                        newReservation = Reservation.Reservation(idDemandeur, idSalle, date, duree)
                        newReservation.calculMontant(salle, duree)

                        #Ajout de la réservation dans le planning
                        self.resaArray.append(newReservation)

                        print"\nReservation creee avec succes"

                else : #non existDemandeur
                    code_retour = 2 #Id demandeur inexistant
                    print "\nImpossible de creer la reservation, veuillez saisir un identifiant de demandeur existant"

            else : #non existSalle
                code_retour = 3 #Id salle inexistant
                print "\nImpossible de creer la reservation, veuillez saisir un identifiant de salle existant"

        return code_retour

    def deleteReservation(self, idSalle, date, duree) :
        idResa = str(idSalle)+str(date)+str(duree)
        dateResa = datetime.datetime.strptime(str(date), '%Y%m%d')
        for s in self.resaArray :
            if (s.getId() == idResa):
                print "\nSuppression de la réservation du "+dateResa.strftime('%d %b %Y')+" pour la "+str(duree)+" concernant la salle "+str(idSalle)
                self.resaArray.remove(s)
                return 0 #OK
        print "\nSuppression impossible, aucune réservation le "+dateResa.strftime('%d %b %Y')+" pour la "+str(duree)+" concernant la salle "+str(idSalle)
        return 1 #Resa inexistant     


    def getReservation(self, idResa):
        for s in self.resaArray :
            if (s.getId() == idResa):
                return s

    def getAllReservations(self):
        reservationArray = []
        for resa in self.resaArray:
            reservationArray.append(resa)
        return reservationArray

    def getAllIdReservations(self):
        reservationIdArray = []
        for resa in self.resaArray:
            reservationIdArray.append(resa.id)
        return reservationIdArray

    def existReservation(self, idResa):
        reservationIdArray = self.getAllIdReservations()
        for resaId in reservationIdArray :
            if (idResa == resaId) :
                return True
        return False

    def printAllReservations(self):
        stringListeResa = ""
        for resa in self.resaArray:
            dateResa = datetime.datetime.strptime(str(resa.date), '%Y%m%d')#Conversion au format date
            stringListeResa = stringListeResa+str("Salle num "+str(resa.idSalle)+" -> reservee le  "+dateResa.strftime('%d %b %Y')+" pour la  "+str(resa.duree+" ==> Montant : "+str(resa.montant)+"€")+"\n")
        print str("\n*********************************PLANNING********************************\n"+stringListeResa)