#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import GestionBatiments
import GestionDemandeurs
import GestionReservations
import GestionMateriels


class GestionGenerale :

	def __init__(self):
		self.gestionBatiments = GestionBatiments.GestionBatiments()
		self.gestionDemandeurs = GestionDemandeurs.GestionDemandeurs()
		self.gestionReservations = GestionReservations.GestionReservations()
		self.gestionMateriels = GestionMateriels.GestionMateriels()

	def getGestionBatiments(self):
		return self.gestionBatiments

	def getGestionDemandeurs(self):
		return self.gestionDemandeurs

	def getGestionReservations(self):
		return self.gestionReservations

	def getGestionMateriels(self):
		return self.gestionMateriels

	def addReservation(self, idDemandeur, idSalle, date, duree):
		idSalleArray = self.gestionBatiments.getAllIdSalles()
		idDemandeurArray = self.gestionDemandeurs.getAllIdDemandeurs()

		existSalle = self.gestionBatiments.existSalle(idSalle)
		existDemandeur = self.gestionDemandeurs.existDemandeur(idDemandeur)

		salle = self.gestionBatiments.getSalle(idSalle)

		return self.gestionReservations.addReservation(idDemandeur, idSalle, date, duree, idSalleArray, idDemandeurArray, existSalle, existDemandeur, salle)
