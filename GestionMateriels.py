#!/usr/bin/python2.7
# -*- coding: utf-8 -*-


class GestionMateriels :
    """Classe permettant la gestion des demandeurs, caractérisée par :
    - une liste de demandeurs """

    typeMateriel = ['whiteboard','retro-projecteur','autres moyens video']

    def __init__(self):
		self.materielMobileArray = []

    def addTypeMateriel(self,type):
      if type in GestionMateriels.typeMateriel:
        print "Ajout impossible du type de Materiel car déjà présent "+type
        return 1
      else:
        GestionMateriels.typeMateriel.append(type)
        print "Ajout du type de Materiel "+type
        return 0

    def printAllTypeMateriel(self):
        print GestionMateriels.typeMateriel

    def removeTypeMateriel(self,type):
        if type in GestionMateriels.typeMateriel:
            GestionMateriels.typeMateriel.remove(type)
            print "Suppresion du type de Materiel "+type
            return 0
        else:
            print "Suppression impossible le type de Materiel "+type+" n'existe pas"
            return 1