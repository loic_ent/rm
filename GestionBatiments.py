#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import Batiment
import Salle
import Materiel

class GestionBatiments:
    """Classe permettant la gestion des batiments, caractérisée par :
    - une liste de bâtiments """

    def __init__(self):
        self.batimentArray = []


    #CRUD Batiment
    #appel constructeur classe bat

    #Recherche Salle
    #Appel la fonction de recherche de salle de la classe batiment 
    #(sur le batiment voulu : en fonction de l'id batiment contenu dans l'id salle)



    def addBatiment(self,num, nom, adr_num, adr_rue, adr_cp, adr_ville):
        if (num in self.getAllIdBatiments()):
            print ("\nImpossible d'ajouter le batiment, l'identifiant saisi est deja existant")
            return 1 #Erreur id existant
        else:
            self.batimentArray.append(Batiment.Batiment(num,nom,adr_num,adr_rue,adr_cp,adr_ville))
            print ("\nBatiment ajoute avec succes")
            return 0 #OK


    def deleteBatiment(self, idBatiment):
        for b in self.batimentArray :
            if (b.getId() == idBatiment):
                print "\nSuppression du batiment "+str(idBatiment)
                self.batimentArray.remove(b)
                return 0 #OK
        print "\nSuppression impossible, le batiment "+str(idBatiment)+" n'existe pas"   
        return 1 #Bat inexistant     

    #salle_type : int ({1:'cuisine', 2:'vaisselle uniquement', 3:'video', 4:'tableau', 5:'amphitheatre'})
    def addSalle(self,salle_etage, salle_num, salle_bat, salle_type, superficie, materiel, cuisine):
        for b in self.batimentArray:
            if b.getId() == salle_bat:
                return b.addSalle(salle_etage, salle_num, salle_type, superficie, materiel, cuisine)

    def deleteSalle(self, idSalle):
        split = idSalle.split("-")
        idBat = split[2]
        for batiment in self.batimentArray :
            if (batiment.getSalle(idSalle) != None) :
                print "\nSuppression de la salle "+str(idSalle)
                self.getBatiment(int(idBat)).sallesArray.remove(self.getSalle(idSalle))
                return 0 #OK
        print "\nSuppression impossible, la salle "+str(idSalle)+" n'existe pas"   
        return 1 #Salle inexistante   

    def printAllBatiments(self):
        for b in self.batimentArray:
            print b

    def getAllBatiments(self):
        batimentArray = []
        for batiment in self.batimentArray :
            batimentArray.append(batiment)
        return batimentArray

    def getAllIdBatiments(self):
        batimentIdArray = []
        for batiment in self.batimentArray :
            batimentIdArray.append(batiment.id)
        return batimentIdArray

    def getBatiment(self, idBatiment):
        for b in self.batimentArray :
            if (b.getId() == idBatiment):
                return b

    def rechercherBatiment(self, idBatiment):
        for b in self.batimentArray :
            if (b.getId() == idBatiment):
                print "\nBatiment trouvé : "
                print b
                return 0 #OK
        print "\nLe batiment "+str(idBatiment)+" n'existe pas"
        return 1 # non trouvé

    def getAllSalles(self):
        salleArray = []
        for batimen in self.batimentArray :
            salleArray.append(batiment.getAllSalles)
        return salleArray

    def getAllIdSalles(self):
        salleIdArray = []
        for batiment in self.batimentArray :
            salleIdArray.append(batiment.getAllIdSalles())
        return salleIdArray

    def existSalle(self, idSalle):
        salleIdArrayArray = self.getAllIdSalles()
        for salleIdArray in salleIdArrayArray :
            for salleId in salleIdArray :
                if (str(idSalle) == str(salleId)) :
                    return True
        return False

    def getSalle(self, idSalle):
        for batiment in self.batimentArray :
            if (batiment.getSalle(idSalle) != None) :
                return batiment.getSalle(idSalle)


    def addMaterielToSalle(self,etage, num, bat, type):
        idSalle = str(etage)+"-"+str(num)+"-"+str(bat)
        newMat = Materiel.Materiel(type)
        if self.existSalle(idSalle):
            salle=self.getSalle(idSalle)
            salle.addMateriel(type)
            print "\nAjout du materiel "+type+" à la salle "+idSalle
            return 0
        else:
            print "\nAjout impossible du materiel dans la salle "+idSalle+" car salle inexistante"
            return 1

    def removeMaterielFromSalle(self,etage, num, bat, type):
        idSalle = str(etage)+"-"+str(num)+"-"+str(bat)
        newMat = Materiel.Materiel(type)
        if self.existSalle(idSalle):
            salle=self.getSalle(idSalle)
            salle.removeMateriel(type)
            print "\nSuppression du materiel "+type+" de la salle "+idSalle
            return 0
        else:
            print "\nSuppression impossible du materiel dans la salle "+idSalle+" car salle inexistante"
            return 1

    def listMaterielOfSalle(self,etage, num, bat):
        idSalle = str(etage)+"-"+str(num)+"-"+str(bat)
        if self.existSalle(idSalle):
            salle=self.getSalle(idSalle)
            print str(salle.getMateriel())
            return 0
        else:
            print "\nLister materiel impossible de la salle "+idSalle+" car salle inexistante"
            return 1

    # def removeMaterielFromSalle(salle, mat):

    def rechercherSalle(self, idSalle):
        for batiment in self.batimentArray :
            if (batiment.getSalle(idSalle) != None) :
                print "\nSalle trouvée : "
                print batiment.getSalle(idSalle)
                return 0 #OK
        print "\nLa salle "+str(idSalle)+" n'existe pas"
        return 1 # non trouvé

    def updateMateriel(self, etage, num, bat, materiel):
        idSalle=str(etage)+"-"+str(num)+"-"+str(bat)
        self.getSalle(idSalle).updateMateriel(materiel)

    def updateBatiment(self,num, nom, adr_num, adr_rue, adr_cp, adr_ville):
        if (num in self.getAllIdBatiments()):        
            b=self.getBatiment(num)
            b.setNom(nom)
            b.setAdrNum(adr_num)
            b.setAdrRue(adr_rue)
            b.setAdrCp(adr_cp)
            b.setAdrVille(adr_ville)
            b.setAdresse(adr_num, adr_rue, adr_cp, adr_ville)
            print "\nmise a jour du batiment "+str(num)+" succes"
            return 0 #OK
        else:
            print "\nImpossible de modifier le batiment "+str(num)+" car inexistant"
            return 1 #Erreur id existant