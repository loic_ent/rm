#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import Demandeur

class GestionDemandeurs :
	"""Classe permettant la gestion des demandeurs, caractérisée par :
	- une liste de demandeurs """

	titreDemandeurArray = {1:'particulier', 2:'association', 3:'entreprise'}
	origineDemandeurArray = {1:'résident', 2:'non résident'}

	def __init__(self) :
		self.demandeurArray = []

	#construct

	#CRUD sur les demandeurs

	def addDemandeur(self, prenom, nom, titre, origine):
		code_retour = 0 # Test OK   

        #Le titre choisit doit exister
		if (int(titre)<1 or int(titre)>len(GestionDemandeurs.titreDemandeurArray)):
			print "Création impossible, saisir un chiffre entre 1 et "+str(len(GestionDemandeurs.titreDemandeurArray))+" pour le titre"
			code_retour = 1 #Erreur Titre demandeur
		else :
			titre=GestionDemandeurs.titreDemandeurArray.get(int(titre))

        #L'origine choisie doit exister
		if (int(origine)<1 or int(origine)>len(GestionDemandeurs.origineDemandeurArray)):
			print "Création impossible, saisir un chiffre entre 1 et "+str(len(GestionDemandeurs.origineDemandeurArray))+" pour l'origine"
			code_retour = 2 #Erreur Origine demandeur
		else :
			origine=GestionDemandeurs.origineDemandeurArray.get(int(origine))

        #On n'ajoute le demandeur que si aucune erreur n'a été détectée
		if (code_retour == 0) :
            #Création du demandeur
			newDemandeur = Demandeur.Demandeur(prenom, nom, titre, origine)
			#Ajout du demandeur dans la liste des demandeurs
			self.demandeurArray.append(newDemandeur)
			print "Demandeur -"+str(newDemandeur.id)+"- ("+str(newDemandeur.prenom)+" "+str(newDemandeur.nom)+") ajoute avec succes"

		return code_retour


	def deleteDemandeur(self, idDemandeur) :
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				print "\nSuppression du Demandeur -"+str(idDemandeur)+"- "+str(d.prenom)+" "+str(d.nom)
				self.demandeurArray.remove(d)
				return 0 #OK
		print "\nSuppression impossible, l'identifiant -"+str(idDemandeur)+"- ne correspond à aucun demandeur"
		return 1 #Demandeur inexistant     

	def setNomDemandeur(self, idDemandeur, nom):
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				print "\nModification du Demandeur -"+str(idDemandeur)+"- Nouveau nom : "+str(d.nom)
				self.getDemandeur(idDemandeur).setNom(nom)
				return 0 #OK
		print "\nModification impossible, l'identifiant -"+str(idDemandeur)+"- ne correspond à aucun demandeur"
		return 1 #Demandeur inexistant

	def setPrenomDemandeur(self, idDemandeur, prenom):
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				print "\nModification du Demandeur -"+str(idDemandeur)+"- Nouveau prenom : "+str(d.prenom)
				self.getDemandeur(idDemandeur).setPrenom(prenom)
				return 0 #OK
		print "\nModification impossible, l'identifiant -"+str(idDemandeur)+"- ne correspond à aucun demandeur"
		return 1 #Demandeur inexistant

	def setTitreDemandeur(self, idDemandeur, titre):
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				if (int(titre)<1 or int(titre)>len(GestionDemandeurs.titreDemandeurArray)):
					print "Création impossible, saisir un chiffre entre 1 et "+str(len(GestionDemandeurs.titreDemandeurArray))+" pour le titre"
					return 2 #Erreur Titre demandeur
				else :
					titre=GestionDemandeurs.titreDemandeurArray.get(int(titre))
					self.getDemandeur(idDemandeur).setTitre(titre)
					print "\nModification du Demandeur -"+str(idDemandeur)+"- Nouveau titre : "+str(d.titre)
					return 0 #OK
		print "\nModification impossible, l'identifiant -"+str(idDemandeur)+"- ne correspond à aucun demandeur"
		return 1 #Demandeur inexistant


	def setOrigineDemandeur(self,idDemandeur,origine):
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				if (int(origine)<1 or int(origine)>len(GestionDemandeurs.origineDemandeurArray)):
					print "Création impossible, saisir un chiffre entre 1 et "+str(len(GestionDemandeurs.origineDemandeurArray))+" pour l'origine"
					return 2 #Erreur Origine demandeur
				else :
					origine=GestionDemandeurs.origineDemandeurArray.get(int(origine))
					self.getDemandeur(idDemandeur).setOrigine(origine)
					print "\nModification du Demandeur -"+str(idDemandeur)+"- Nouvelle origine : "+str(d.origine)
					return 0 #OK
		print "\nModification impossible, l'identifiant -"+str(idDemandeur)+"- ne correspond à aucun demandeur"
		return 1 #Demandeur inexistant


	def getDemandeur(self, idDemandeur):
		for d in self.demandeurArray :
			if (d.getId() == idDemandeur):
				return d

	def getAllIdDemandeurs(self) :
		demandeurIdArray = []
		for demandeur in self.demandeurArray :
			demandeurIdArray.append(demandeur.id)
		return demandeurIdArray

	def getAllDemandeurs(self) :
		demandeurArray = []
		for demandeur in self.demandeurArray :
			demandeurArray.append(demandeur)
		return demandeurArray

	def existDemandeur(self, idDemandeur):
		demandeurIdArray = self.getAllIdDemandeurs()
		for demandeurId in demandeurIdArray :
			if (idDemandeur == demandeurId) :
				return True
		return False

	def printAllDemandeurs(self):
		for d in self.demandeurArray:
			print d