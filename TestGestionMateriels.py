#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import unittest

import GestionGenerale

class TestGestionMateriels(unittest.TestCase): #Attention, les fonctions de test s'exécutent dans l'ordre alphabétique

    gg=GestionGenerale.GestionGenerale()

#Test de la fonction d'ajout du type de materiel :

    def test1_addTypeMateriel(self):
        #Ajouts de Demandeurs
        res = TestGestionMateriels.gg.getGestionMateriels().addTypeMateriel("Xbox")
        self.assertEqual(res,0)

        res = TestGestionMateriels.gg.getGestionMateriels().addTypeMateriel("Xbox")
        self.assertEqual(res,1)

    def test2_affichage(self):
        print ('\n=========================================================================')
        TestGestionMateriels.gg.getGestionMateriels().printAllTypeMateriel()
        print ('=========================================================================\n')


    def test3_removeTypeMateriel(self):
        #Ajouts de Demandeurs
        res = TestGestionMateriels.gg.getGestionMateriels().removeTypeMateriel("Xbox")
        self.assertEqual(res,0)

        res = TestGestionMateriels.gg.getGestionMateriels().removeTypeMateriel("Xbox")
        self.assertEqual(res,1)

    def test4_affichage(self):
        print ('\n=========================================================================')
        TestGestionMateriels.gg.getGestionMateriels().printAllTypeMateriel()
        print ('=========================================================================\n')

if __name__ == '__main__':
    unittest.main()