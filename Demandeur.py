#!/usr/bin/python2.7
# -*- coding: utf-8 -*-


class Demandeur :
	
	i = int(1) # incrément pour les identifiants

	def __init__(self, prenom, nom, titre, origine):
		self.id = Demandeur.i # int
		Demandeur.i+=1
		self.prenom = prenom #string 
		self.nom = nom #string
		self.titre = titre #string #{1:'particulier', 2:'association', 3:'entreprise'}
		self.origine = origine #string #{1:'résident', 2:'non résident'}

	def getId(self):
		return self.id

	def setPrenom(self, prenom):
		self.prenom = prenom


	def setNom(self, nom):
		self.nom = nom


	def setTitre(self, titre):
		self.titre = titre


	def setOrigine(self, origine):
		self.origine = origine


	def __str__(self):
		return str("id "+str(self.id)+" : "+str(self.prenom)+" "+str(self.nom)+" - titre : "+str(self.titre)+" - origine : "+str(self.origine))